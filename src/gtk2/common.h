/*  XRoar - a Dragon/Tandy Coco emulator
 *  Copyright (C) 2003-2015  Ciaran Anscomb
 *
 *  See COPYING.GPL for redistribution conditions. */

#ifndef XROAR_GTK2_COMMON_H_
#define XROAR_GTK2_COMMON_H_

#include <gtk/gtk.h>

gboolean gtk2_dummy_keypress(GtkWidget *, GdkEventKey *, gpointer);

#endif  /* XROAR_GTK2_COMMON_H_ */
